import glob
import os
import itertools
import numpy as np
import cv2
import random
from keras.models import load_model
import keras.backend as K
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd


class FeatureVectorExtractor:


    def __init__(self, model_path, layer_name='global_average_pooling2d_1', runs=1):
        '''

        Creates a new feature vector extractor

        :param model_path: path to the model to use
        :param layer_name: the layer to get the feature vector data off of
        :param runs: how many times to average the feature vector data over. 1 = no averaging
        '''

        self.model = load_model(model_path)
        self.layer_name = layer_name
        self.runs = runs


    def extract_feature_vector(self, image_path):
        '''
        Returns the feature vector representation of the image

        :param image_path:
        :return:
        '''

        layer = self.model.get_layer(name=self.layer_name)
        img = np.expand_dims(cv2.imread(image_path), axis=0) / 255

        # Variables for easy access to needed layers
        input = self.model.layers[0].input
        output = layer.output

        # A function that takes in the input layer and outputs data after the given layer
        get_output = K.function([input], [output])

        # run function
        return get_output([img])[0].flatten()


    def get_averaged_fv_data(self, num_replicates, data_dir):
        '''
        Returns a matrix where each row contains the class name and the associated averaged feature vector data

        :param num_replicates: how many averaged feature vectors to get per class
        :param data_dir: a folder whose subfolders correspond to images of different classes
        :return:
        '''

        image_paths = []
        # get list of tuples containing class name and the image path to use for the class
        for p in self._get_image_paths_generator(num_replicates, data_dir):
            image_paths += p

        # each class has a list of class_points*runs amount of convolved data
        class_to_convolved_mapping = self._get_fv_data(image_paths)

        # the convolved data in order, each class row averaged over its convolved data
        averaged_fv_data = None
        data_labels = []
        for c in os.listdir(data_dir):
            # split into "runs" number of evenly sized chunks of length
            _ = np.array_split(class_to_convolved_mapping[c]['fv_data'], self.runs)
            # average over each
            _ = np.average(_, axis=0)
            averaged_fv_data = _ if averaged_fv_data is None else np.vstack((averaged_fv_data, _))

            # data labels has `num_replicates` number of class name
            data_labels += [c] * num_replicates

        data_labels = np.expand_dims(np.array(data_labels), axis=1)

        return np.hstack((data_labels, averaged_fv_data))


    def _get_fv_data(self, image_paths):
        '''

        Returns a dictionary of classes mapping to a list of image paths and feature vector data

        :param image_paths: a list of tuples (class name, image path) to generate feature vector data for
        :return:
        '''

        # dictionary of classes mapping to a list of image paths and feature vector data
        class_to_fv_mapping = {}

        # go through each image path, get the
        for c, i_p in image_paths:
            fv = self.extract_feature_vector(i_p)

            if c not in class_to_fv_mapping:
                class_to_fv_mapping[c] = {'paths': [], 'fv_data': []}

            class_to_fv_mapping[c]['paths'].append(i_p)
            class_to_fv_mapping[c]['fv_data'].append(fv)

        return class_to_fv_mapping


    def _get_image_paths_generator(self, num_replicates, data_dir, img_extension='.jpg'):
        '''

        Returns a list of tuples containing class name and the image path to use for the class

        :param num_replicates: how many averaged feature vectors to get per class
        :param data_dir: a list of data directories to sample their images for convolved image generation
        :return: list of tuples
        '''

        # dict of class and image paths to use for the class
        image_paths_mapping = {}

        for c in os.listdir(data_dir):

            class_folder = os.path.join(data_dir, c)

            # get all image paths
            paths = glob.glob(os.path.join(class_folder, '*' + img_extension))

            if len(paths) < num_replicates:
                raise Exception(
                    "Cannot generate {} data; only {} tiles total were found in the dir for {}".format(
                        num_replicates,
                        len(paths), c))

            image_paths_mapping[c] = paths

        # seed for randomness starting from 0
        for seed in range(self.runs):
            # tuples of class name and path(s) to use
            image_paths_tuples = []

            for c in os.listdir(data_dir):
                # get random subset of paths for this class
                curr_class_paths = random.Random(seed).sample(image_paths_mapping[c], num_replicates)

                # add class and paths to use for this class
                image_paths_tuples += list(zip(itertools.repeat(c), curr_class_paths))

            yield image_paths_tuples


def make_clustermap(fv_data, data_labels, out_dir, color_track_setting):
    '''

    Performs clustering on the feature vector data and saves a colorcoded clustermap

    :param fv_data:
    :param data_labels:
    :param out_dir:
    :param color_track_setting:
    :return:
    '''

    colors = {
        'red': (230, 25, 75), 'green': (60, 180, 75), 'yellow': (255, 225, 25),
        'blue': (0, 130, 200), 'orange': (245, 130, 48), 'purple': (145, 30, 180),
        'cyan': (70, 240, 240), 'magenta': (240, 50, 230), 'lime': (210, 245, 60),
        'pink': (250, 190, 190), 'teal': (0, 128, 128), 'lavender': (230, 190, 255),
        'brown': (170, 110, 40), 'beige': (255, 250, 200), 'maroon': (128, 0, 0),
        'mint': (170, 255, 195), 'olive': (128, 128, 0), 'apricot': (255, 215, 180),
        'navy': (0, 0, 128), 'grey': (128, 128, 128), 'black': (0, 0, 0)
    }

    scaled_fv_data = MinMaxScaler(copy=False).fit_transform(fv_data)

    # mapping each folder in `data_dirs` to a color for color track purposes
    color_track = {}
    for c in set(data_labels):
        pref = c.split('-')[0].strip()
        color_track[c] = [colors[color_track_setting[pref]]]

    for c in color_track:
        for i, tup in enumerate(color_track[c]):
            color_track[c][i] = tuple(_ / 255. for _ in tup)

    # setting up color track
    colors = []
    for dl in data_labels:
        colors.append(color_track[dl])
    colors = pd.DataFrame(colors)
    colors = [colors[0]]

    # perform clustering and save
    plt.close('all')
    df = pd.DataFrame(scaled_fv_data, index=data_labels)
    sns.clustermap(df, method='complete', row_colors=colors)

    save_name = 'clustering_heatmap.jpg'
    plt.savefig(os.path.join(out_dir, save_name), dpi=1000, bbox_inches='tight')


if __name__ == '__main__':
    model_path = '../models/codel_model.h5'
    data_dir = '../data/sample cases'
    num_replicates = 1
    average_over = 20
    out_dir = '../results'

    # assign colors to each folder prefix within the `data_dirs`. available colors specified above
    # folder prefix name should following: PREFIX - NAME
    color_track_setting = {
        '1': 'red',
        '3': 'yellow',
        '4': 'blue',
        '5': 'orange',
        '6': 'purple'
    }

    fve = FeatureVectorExtractor(model_path, runs=average_over)
    res = fve.get_averaged_fv_data(num_replicates, data_dir)
    data_labels, fv_data = res[:, 0], res[:, 1:]

    make_clustermap(fv_data, data_labels, out_dir, color_track_setting)
