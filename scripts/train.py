from pathlib import Path
import os
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense
from keras.layers import GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import multi_gpu_model
from keras.applications import VGG19
import cv2

from .train_helper import get_class_weights


def train():
    num_gpus_to_use = 1
    num_blocks_to_train = 12  # number of CNN blocks we want to train
    save_path = r'path'

    tile_size = 1024
    batch_size = 4
    epochs = 200

    train_dir = r'path'
    val_dir = r'path'


    # pretrained model is trained using BGR
    def flipRGB(image):
        return cv2.cvtColor(image, cv2.COLOR_RGB2BGR)


    # augmentation of images settings
    image_data_gen_configs = {
        'rescale': 1 / 255,
        'rotation_range': 360,
        'width_shift_range': 0.2,
        'height_shift_range': 0.2,
        'zoom_range': 0.2,
        'horizontal_flip': True,
        'vertical_flip': True,
        'preprocessing_function': flipRGB
    }

    #######################################################################################################################

    classes = os.listdir(train_dir)

    num_train_images = len(list(Path(train_dir).glob('**/*'))) - len(classes)
    num_test_images = len(list(Path(val_dir).glob('**/*'))) - len(classes)

    # loading images/image augmentation
    train_gen = ImageDataGenerator(**image_data_gen_configs)
    val_gen = ImageDataGenerator(rescale=1 / 255, preprocessing_function=flipRGB)

    # training images
    train_generator = train_gen.flow_from_directory(
        train_dir,
        target_size=tile_size,
        classes=classes,
        batch_size=batch_size,
        class_mode='categorical')

    # validation images
    validation_generator = val_gen.flow_from_directory(
        val_dir,
        target_size=tile_size,
        classes=classes,
        batch_size=batch_size,
        class_mode='categorical')

    my_model = VGG19(include_top=False)

    x = my_model.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(len(classes), activation='softmax')(x)
    model = Model(my_model.input, predictions)

    if num_gpus_to_use > 1:
        # Instantiate the base model (or "template" model).
        # We recommend doing this with under a CPU device scope,
        # so that the model's weights are hosted on CPU memory.
        # Otherwise they may end up hosted on a GPU, which would
        # complicate weight sharing.
        parallel_model = multi_gpu_model(model, gpus=num_gpus_to_use)  # multi gpu
    else:
        parallel_model = model

    # train layers. this is done on base model NOT PARALLEL
    for layer in model.layers[:num_blocks_to_train]:
        layer.trainable = False
    for layer in model.layers[num_blocks_to_train:]:
        layer.trainable = True

    from keras.optimizers import SGD

    parallel_model.compile(optimizer=SGD(lr=0.0001, momentum=0.9), loss='categorical_crossentropy',
                           metrics=['accuracy'])

    # checkpoint callback setup
    checkpoint = ModelCheckpoint(save_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')

    print('\nFinetuning top block(s)\n')

    # we train our model again alongside the top Dense layers
    parallel_model.fit_generator(
        train_generator,
        steps_per_epoch=num_train_images // batch_size,
        epochs=epochs,
        verbose=2,
        callbacks=[checkpoint],
        validation_data=validation_generator,
        validation_steps=num_test_images // batch_size,
        class_weight=get_class_weights(train_dir)  # weighted train to better handle any class imbalance
    )


if __name__ == '__main__':
    train()
