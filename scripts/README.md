# Feature Vector Extraction and Clustering

[![Open in Code Ocean](https://codeocean.com/codeocean-assets/badge/open-in-code-ocean.svg)](https://codeocean.com/capsule/9202998/tree)


Specify at the bottom of `feature_vector_clustermap.py` the following key parameters:

- Data consists of a directory of class folders containing image tiles.

- The number of replicates to use specifies how many feature vectors to use per class on the clustermap

- The number of tiles to average over specifies how many times to average the feature vector data over (1 = no averaging)

- The color track settings specifies the color to assign each class on the clustermap. If there are multiple class folders belonging to the same class, you can group class folders using a hyphenated prefix so that they have the same colortrack color. (The supplied sample cases data demonstrates the format)

Output is a colorcoded clustermap


# Feature Activation Map (FAM) Generator

[![Open in Code Ocean](https://codeocean.com/codeocean-assets/badge/open-in-code-ocean.svg)](https://codeocean.com/capsule/0685076/tree)

Specify at the bottom of `feature_map_generator.py` the following key parameters:

- Data consists of a directory of image tiles to generate FAM on.

- Feature number is the feature number of interest to generate the FAM

Output is a FAM of each of the tiles
