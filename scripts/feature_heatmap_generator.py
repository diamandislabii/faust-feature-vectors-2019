import os
import cv2
import numpy as np
import keras.backend as K
from keras.models import load_model


class FeatureHeatmapGenerator:


    def __init__(self, model_path, alpha=0.6, conv_name='block5_conv4'):
        self.model = load_model(model_path)
        self.alpha = alpha
        self.conv_name = conv_name


    def heatmap_of_feature(self, image_path, feature_number, save_dir):
        '''
        Saves the feature heatmap of the given image

        :param image_path:
        :param feature_number:
        :param save_dir:
        :return:
        '''

        fam = self._heatmap_of_feature_helper(image_path, feature_number)
        save_path = os.path.join(save_dir, '{:d}_{}.jpg'.format(feature_number, os.path.basename(image_path)))
        cv2.imwrite(save_path, fam.astype(np.uint8, copy=False))


    def _heatmap_of_feature_helper(self, image_path, feature_number):
        '''
        Returns the passed in image with a heatmap of the specific feature number overlayed

        :param model_path:
        :param image_path:
        :param feature_number:
        :param conv_name:
        :return:
        '''

        image = cv2.imread(image_path)
        width, height, _ = image.shape

        final_conv_layer = self.model.get_layer(name=self.conv_name)
        get_output = K.function([self.model.layers[0].input], [final_conv_layer.output])
        conv_output = np.squeeze(get_output([np.expand_dims(image / 255, axis=0)])[0])

        # generate heatmap
        cam = conv_output[:, :, feature_number - 1]
        cam /= np.max(cam)
        cam = cv2.resize(cam, (height, width))
        heatmap = cv2.applyColorMap(np.uint8(255 * cam), cv2.COLORMAP_JET)

        # overlay heatmap on original image
        return cv2.addWeighted(image, self.alpha, heatmap, 1 - self.alpha, 0)


if __name__ == '__main__':
    model_path = '../models/codel_model.h5'
    image_path = r'../data/image.jpg'
    feature_number = 66
    out_dir = r'../results/'

    ####################################################################################################################

    fhg = FeatureHeatmapGenerator(model_path)
    fhg.heatmap_of_feature(image_path, feature_number, out_dir)
